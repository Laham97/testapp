import { makeStyles } from "@material-ui/core/styles";
import DOCORS from "../../../assets/images/Doctors.png";

export const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: `url(${DOCORS})`,
    backgroundRepeat: "no-repeat",
  },
  paper: {
    margin: theme.spacing(6, 2),
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    [theme.breakpoints.down("xs")]: {
      alignItems: "center",
    },
  },

  form: {
    width: "82%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  input: {
    borderRadius: 13,
  },
  checkBox: {
    borderRadius: 20,
  },
  button: {
    backgroundColor: "#2581C5",
  },
  mainGrade: {
    marginLeft: "5rem",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0rem",
    },
  },
  Row: {
    display: "flex",
    justifyContent: "space-around",
    flexDirection: "row",
    marginBottom: 10,
  },
  RowSpace: {
    display: "flex",
    justifyContent: "space-evenly",
    flexDirection: "row",
    marginBottom: 10,
    marginTop: 20,
  },
}));
