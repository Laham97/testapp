import React from "react";
import Button from "@material-ui/core/Button";
import { Divider, Hidden } from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import { Logo } from "../../../constants";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { ProgressComponent } from "../../../components";
import Typography from "@material-ui/core/Typography";
import { useStyles } from "./style";
import { Inputs } from "./lib";
function Copyright() {
  return (
    <Typography variant="subtitle2" align="center">
      Already registered? Login here
    </Typography>
  );
}

export const SignInSide = () => {
  const classes = useStyles();
  const [showPass, setShowPass] = React.useState(false);

  return (
    <Grid container component="main" className={classes.root}>
      <Hidden smDown>
        <Grid item xs={false} sm={4} md={5} className={classes.image} />
      </Hidden>
      <Grid item xs={12} sm={8} md={6} lg={4} className={classes.mainGrade}>
        <div className={classes.paper}>
          <Logo style={{ marginBottom: "4rem" }} />
          <Typography variant="h5">Register now</Typography>
          <Typography variant="h6">
            Lorem ipsum dolor sit amet, consectetur adipiscing.
          </Typography>
          <form className={classes.form} noValidate>
            <Inputs showPass={showPass} setShow={setShowPass} />
            <ProgressComponent />
            <Typography variant="h6" color="primary">
              Verify your account with
            </Typography>
            <div className={classes.Row}>
              <FormControlLabel
                control={
                  <Checkbox
                    value="Phone number"
                    classes={{
                      root: classes.checkBox,
                    }}
                    color="primary"
                  />
                }
                label="Phone number"
              />
              <FormControlLabel
                control={<Checkbox value="Email" color="primary" />}
                label="Email"
              />
            </div>
            <Divider />
            <div className={classes.RowSpace}>
              <FormControlLabel
                control={<Checkbox value="Email" color="primary" />}
                label="Email"
              />
              <Typography variant="h6" color="primary">
                I accept the terms and conditions, consectetur adipiscing elit.
                Integer eu velit est. Maecenas nulla justo, feugiat eget
                <a href="/google"> congue in,</a>, auctor tellus.
              </Typography>
            </div>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              classes={{
                root: classes.button,
              }}
              className={classes.submit}
            >
              <Typography variant="button">Continue</Typography>
            </Button>

            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
};
