import React from "react";
import { TextField, InputAdornment, IconButton } from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { useStyles } from "../style";
interface Props {
  showPass: boolean;
  setShow: (s: boolean) => void;
}

export const Inputs: React.FC<Props> = ({ showPass, setShow }) => {
  const classes = useStyles();
  return (
    <>
      <TextField
        variant="filled"
        margin="normal"
        required
        fullWidth
        id="fullname"
        label="Full name"
        name="fullname"
        autoFocus
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.input,
          },
        }}
      />
      <TextField
        variant="filled"
        margin="normal"
        required
        fullWidth
        id="Email"
        label="Email"
        name="Email"
        autoFocus
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.input,
          },
        }}
      />
      <TextField
        variant="filled"
        margin="normal"
        required
        fullWidth
        id="Mobile number"
        label="Mobile number"
        name="Mobile number"
        autoFocus
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.input,
          },
        }}
      />
      <TextField
        variant="filled"
        margin="normal"
        required
        fullWidth
        type={showPass ? "text" : "password"}
        id="Password"
        label="Password"
        name="Password"
        autoFocus
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.input,
          },
          endAdornment: (
            <InputAdornment position="end">
              {showPass ? (
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShow(!showPass)}
                >
                  <Visibility />
                </IconButton>
              ) : (
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setShow(!showPass)}
                >
                  <VisibilityOff />
                </IconButton>
              )}
            </InputAdornment>
          ),
        }}
        style={{ borderRadius: 30 }}
      />
    </>
  );
};
