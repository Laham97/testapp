import { createTheme } from "@material-ui/core/styles";
export const lightTheme = createTheme({
  palette: {
    type: "light",
    background: {
      default: "#FFFFFF",
    },
    text: {
      secondary: "#7B91A6",
      primary: "#23496B",
    },
    primary: {
      main: "#225089",
    },
    secondary: {
      main: "#01A9B4",
    },
  },
  typography: {
    // fontFamily: ["Poppins", "cursive"].join(","),
    h1: {
      fontSize: 92,
      fontWeight: 300,
    },
    h2: {
      fontSize: 58,
      fontWeight: 300,
    },
    h3: {
      fontSize: 43,
    },
    h4: {
      fontSize: 33,
      fontWeight: 400,
    },
    h5: {
      fontSize: 28,
      fontWeight: 400,
      fontStyle: "bold",
    },
    h6: {
      fontSize: 16,
      fontWeight: 500,
    },
    subtitle1: {
      fontSize: 14,
      fontWeight: 500,
    },
    subtitle2: {
      fontSize: 16,
      fontWeight: 500,
      color: "#2581C5",
    },
    button: {
      fontWeight: 500,
      fontSize: 16,
      color: "#FFF",
    },
  },
});
