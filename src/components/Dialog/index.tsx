import React from "react";
import {
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { Transition } from "./transation";
import { useStyles } from "./style";

interface Props {
  handleOpen: () => void;
  handleClose: () => void;
  open: boolean;
  setOpen: (o: boolean) => void;
  children?: React.ReactElement;
  name?: any;
  action?: any;
  fS: boolean;
  max?: any;
}

export const DialogComponent: React.FC<Props> = ({
  handleClose,
  handleOpen,
  open,
  setOpen,
  children,
  name,
  action,
  fS,
  max,
}) => {
  const classes = useStyles();
  return (
    <>
      <Dialog
        fullScreen={fS}
        maxWidth={max}
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="primary"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {name}
            </Typography>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    </>
  );
};
