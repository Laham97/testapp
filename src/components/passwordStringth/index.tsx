import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { LinearProgress, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});

export const ProgressComponent = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <LinearProgress variant="determinate" color="secondary" value={50} />
      <div
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          display: "flex",
        }}
      >
        <Typography variant="subtitle1" color="textSecondary">
          Password strenght
        </Typography>
        <Typography variant="subtitle1" color="secondary">
          OK
        </Typography>
      </div>
    </div>
  );
};
