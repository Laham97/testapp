import "./App.css";
import { SignInSide } from "./pages";
const App = () => {
  return (
    <div style={{ height: "100vh" }}>
      <SignInSide />
    </div>
  );
};

export default App;
